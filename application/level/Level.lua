require 'lust.utilities.Class'
require 'Tserial'

local List = require 'lust.utilities.List'
local Point = require 'lust.geometry.Point'
local Ball = require 'entities.Ball'
local House = require 'entities.House'

local Level = Class(function(self, name)
	self.directory = '/assets/levels/'
	self.name = name
	self:convertOutputData(nil)
end)

function Level:getTiles()
	return self.data.tiles
end

function Level:setTiles(arr)
	self.data.tiles = arr
end

function Level:setPlayerPosition(player, map)
	self.data.playerPosition = map:getIndexAtPosition(Point(player.x, player.y))
end

function Level:getPlayerPosition(map)
	return map:getPositionAtIndex(self.data.playerPosition)
end

function Level:getBalls(map)
	local arr = {}
	for i, v in ipairs(self.data.balls) do
		local ball = Ball()
		local bp = map:getPositionAtIndex(v)
		ball.x = bp.x
		ball.y = bp.y
		arr[i] = ball
	end
	return arr
end

function Level:setBalls(list, map)
	local arr = {}
	for i=1, list:size(), 1 do
		local ball = list:get(i)
		arr[i] = map:getIndexAtPosition(Point(ball.x, ball.y))
	end
	self.data.balls = arr
end

function Level:getHouses(map)
	local arr = {}
	for i, v in ipairs(self.data.houses) do
		local house = House()
		local hp = map:getPositionAtIndex(v)
		house.x = hp.x
		house.y = hp.y
		arr[i] = house
	end
	return arr
end

function Level:setHouses(list, map)
	local arr = {}
	for i=1, list:size(), 1 do
		local house = list:get(i)
		arr[i] = map:getIndexAtPosition(Point(house.x, house.y))
	end
	self.data.houses = arr
end

function Level:createDirectory()
	if not ( love.filesystem.exists(self.directory) and love.filesystem.isDirectory(self.directory) ) then
		love.filesystem.mkdir(self.directory)
	end
end

function Level:write()
	self:createDirectory()
	local contents = Tserial.pack(self.data, nil, false)
	local file = love.filesystem.newFile(self.directory .. self.name)
	file:open("w")
	file:write(contents)
	file:close()
end

function Level:read()
	self:createDirectory()
	if love.filesystem.exists(self.directory .. self.name) then
	    local file = love.filesystem.newFile(self.directory .. self.name)
		if file:open("r") then
			contents, size = file:read(file:getSize())
			file:close()
			self:convertOutputData(Tserial.unpack(contents, output))
		else
			print("Could not open level: " .. self.directory .. self.name)
		end
	end
end

function Level:convertOutputData(output) 
	self.data = {}
	if output == nil then
		self.data.tiles = {}
		self.data.playerPosition = 0
		self.data.balls = {}
		self.data.houses = {}
	else
		self.data.tiles = output.tiles or {}
		self.data.playerPosition = output.playerPosition or 0
		self.data.balls = output.balls or {}
		self.data.houses = output.houses or {}
	end
end

return Level