local Scene = require "lust.Scene"
local List = require "lust.utilities.List"
local Point = require "lust.geometry.Point"
local Rectangle = require "lust.geometry.Rectangle"
local Keyboard = require "lust.input.Keyboard"
local Button = require "lust.templates.ui.Button"
local Picture = require "lust.graphics.Picture"
local Level = require "level.Level"
local Preview = require "entities.Preview"

local LevelSelectScene = Class(Scene, function(self)
	Scene.init(self)
	
	self.index = 1
	self.currentLevel = nil
	self.levels = self:getLevels()

	self.preview = Preview()
	self.preview.x = 200
	self.preview.y = 60
	self:add(self.preview)

	self.playBtn = Button(Picture(self.textures:aget("assets/player.png")))
	self.editBtn = Button(Picture(self.textures:aget("assets/ball.png")))
	self:add(self.playBtn)
	self:add(self.editBtn)

	self:displayLevel(self.index)
end)

function LevelSelectScene:focusIn()
	Scene.focusIn(self)
	self.playBtn.x = self.camera.width - (self.playBtn.hitbox.width * 3)
	self.playBtn.y = self.playBtn.hitbox.height
	self.editBtn.x = self.playBtn.x
	self.editBtn.y = self.playBtn.y + self.playBtn.frame.width
end

function LevelSelectScene:update(dt)
	Scene.update(self, dt)

	if self.playBtn.clicked and self.currentLevel ~= nil then 
		local GameScene = require "scenes.GameScene"
		self.process:show(GameScene(self.currentLevel))
		return
	elseif self.editBtn.clicked and self.currentLevel ~= nil then
		local EditorScene = require "scenes.EditorScene"
		self.process:show(EditorScene(self.currentLevel))
		return
	end

	local i = self.index
	local k = self.keyboard
	if k:pressed(Keyboard.LEFT) then
		i = i - 1
	elseif k:pressed(Keyboard.RIGHT) then
		i = i + 1
	end

	if i < 1 then
		i = 1
	elseif i > self.levels:size() then
		i = self.levels:size()
	end

	if i ~= self.index then
		self:displayLevel(i)
	end
end

function LevelSelectScene:getLevels()
	local list = List()
	local files = love.filesystem.enumerate("assets/levels")
	for i, file in ipairs(files) do
		list:push(file)
	end
	return list
end

function LevelSelectScene:displayLevel(index)
	self.index = index
	local level = Level(self.levels:get(index))
	level:read()
	self.preview:show(index, level)
	self.currentLevel = level
end

return LevelSelectScene