local Point = require "lust.geometry.Point"
local Rectangle = require "lust.geometry.Rectangle"
local Button = require "lust.templates.ui.Button"
local Picture = require "lust.graphics.Picture"
local GameScene = require "scenes.GameScene"

local EditorScene = Class(GameScene, function(self, level)
	GameScene.init(self, level)
	self.saveBtn = Button(Picture(self.textures:aget("assets/player.png")))
	self:setupGui()
end)

function EditorScene:focusIn()
	GameScene.focusIn(self)
	self.saveBtn.x = self.camera.width - self.saveBtn.frame.width
	self.saveBtn.y = self.restartBtn.y + self.restartBtn.frame.height
end

function EditorScene:save()
	self.level:setTiles(self.map:getTiles())
	self.level:setPlayerPosition(self.player, self.map)
	self.level:setBalls(self.balls, self.map)
	self.level:setHouses(self.houses, self.map)
	self.level:write()
	print("Saved.")
end

function EditorScene:clearGui()
	GameScene.clearGui(self)
	if self.saveBtn then self:remove(self.saveBtn) end
end

function EditorScene:setupGui()
	GameScene.setupGui(self)
	if self.saveBtn then self:add(self.saveBtn) end
end

function EditorScene:addBallAt(point)
	local i = self.map:getIndexAtPosition(point)
	local p = self.map:getPositionAtIndex(i)
	local ball = self:getEntityAtTile(self.balls, i)

	if ball ~= nil then
		self:remove(ball)
		self.balls:slice(self.balls:indexOf(ball))
	else
		ball = Ball()
		ball.x = p.x
		ball.y = p.y
		self.balls:push(ball)
		self:add(ball)
	end
end

function EditorScene:addHouseAt(point)
	local i = self.map:getIndexAtPosition(point)
	local p = self.map:getPositionAtIndex(i)
	local house = self:getEntityAtTile(self.houses, i)
	
	if house ~= nil then
		self:remove(house)
		self.houses:slice(self.houses:indexOf(house))
	else
		house = House()
		house.x = p.x
		house.y = p.y
		self.houses:push(house)
		self:add(house)
	end
end

function EditorScene:updateUi()
	GameScene.updateUi(self)

	if self.saveBtn.clicked then
		self:save()
		return
	end

	local rc = self.mouse.right
	local mc = self.mouse.middle
	local lc = self.mouse.left
	local rect = Rectangle()
	rect.x = self.map.x + self.camera.x
	rect.y = self.map.y + self.camera.y
	rect.width = self.map.tilemap.width
	rect.height = self.map.tilemap.height
	if rect:pointInside(rc.position) and not rc.preventsNext then
		if rc.activated then
			self:addBallAt(Point(rc.position.x, rc.position.y))
			self:updateHousesFilled()
			rc:preventNext()
		end
	end
	if rect:pointInside(mc.position) and not mc.preventsNext then 
		if mc.activated then
			self:addHouseAt(Point(mc.position.x, mc.position.y))
			self:updateHousesFilled()
			mc:preventNext()
		end
	end
	if rect:pointInside(lc.position) and not lc.preventsNext then
		if lc.activated then
			self.map:changeTileAt(self.map:globalToLocal(Point(lc.position.x, lc.position.y)))
			lc:preventNext()
		end
	end
end

return EditorScene