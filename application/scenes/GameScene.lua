local Scene = require "lust.Scene"
local List = require "lust.utilities.List"
local Point = require "lust.geometry.Point"
local Rectangle = require "lust.geometry.Rectangle"
local Keyboard = require "lust.input.Keyboard"
local Button = require "lust.templates.ui.Button"
local Map = require "entities.Map"
local Ball = require "entities.Ball"
local Player = require "entities.Player"
local House = require "entities.House"
local Level = require "level.Level"
local Picture = require "lust.graphics.Picture"
local Hud = require "entities.Hud"

local GameScene = Class(Scene, function(self, level)
	Scene.init(self)

	self.textures:aget("assets/tileset.png")
	self.textures:aget("assets/player.png")
	self.textures:aget("assets/ball.png")
	self.textures:aget("assets/house.png")

	self.level = level

	self.map = Map()
	self.balls = List()
	self.houses = List()
	self.player = Player()

	self.restartBtn = Button(Picture(self.textures:aget("assets/ball.png")))
	self.quitBtn = Button(Picture(self.textures:aget("assets/house.png")))

	self.hud = Hud()

	self:setupLevel()
	self:setupGui()
end)

function GameScene:focusIn()
	Scene.focusIn(self)
	self.quitBtn.x = self.camera.width - self.quitBtn.frame.width
	self.quitBtn.y = 0
	self.restartBtn.x = self.camera.width - self.restartBtn.frame.width
	self.restartBtn.y = self.quitBtn.y + self.quitBtn.frame.height
end

function GameScene:update(dt)
	self:updateUi()
	Scene.update(self, dt)
	self:updatePlayer()
end

function GameScene:clearLevel()
	self:remove(self.map)
	self:remove(self.player)

	for i=1, self.balls:size(), 1 do
		local ball = self.balls:get(i)
		self:remove(ball)
	end
	self.balls:empty()

	for i=1, self.houses:size(), 1 do
		local house = self.houses:get(i)
		self:remove(house)
	end
	self.houses:empty()
end

function GameScene:clearGui()
	self:remove(self.hud)
	self:remove(self.quitBtn)
	self:remove(self.restartBtn)
end

function GameScene:setupLevel()
	self:clearLevel()

	-- tiles
	self.map.x = 40
	self.map.y = 40
	self:add(self.map)
	self.map:setTiles(self.level:getTiles())
	
	-- player
	local p = self.level:getPlayerPosition(self.map)
	self.player.x = p.x
	self.player.y = p.y

	-- houses
	local houses = self.level:getHouses(self.map)
	for i, v in ipairs(houses) do
		self.houses:push(v)
		self:add(v)
	end

	-- balls
	local balls = self.level:getBalls(self.map)
	for i, v in ipairs(balls) do 
		self.balls:push(v)
		self:add(v)
	end

	self:add(self.player)
	self:updateHousesFilled()
end

function GameScene:setupGui()
	self:clearGui()
	self.hud.x = self.map.x
	self.hud.y = self.map.y - 20
	self:add(self.hud)
	self:add(self.restartBtn)
	self:add(self.quitBtn)
end

function GameScene:checkWinCondition()
	local won = true
	for i=1, self.houses:size(), 1 do
		local house = self.houses:get(i)
		if not house.filled then
			won = false
		end
	end

	if won then
		print("won")
	end
end

function GameScene:updateUi()
	if self.restartBtn.clicked then
		self:setupLevel()
		return
	elseif self.quitBtn.clicked then
		local LevelSelectScene = require "scenes.LevelSelectScene"
		self.process:show(LevelSelectScene())
		return
	end
end

function GameScene:updateHousesFilled()
	for i=1, self.houses:size(), 1 do
		local house = self.houses:get(i)
		local index = self.map:getIndexAtPosition(Point(house.x, house.y))
		local ball = self:getEntityAtTile(self.balls, index)
		if ball ~= nil then
			house:fill()
			ball.visible = false
		else
			house:empty()
		end
	end
end

function GameScene:updatePlayer()
	local i = self.map:getIndexAtPosition(Point(self.player.x, self.player.y))
	local bi = i
	local k = self.keyboard

	if k:pressed(Keyboard.LEFT) then
		i = i - 1
	elseif k:pressed(Keyboard.RIGHT) then
		i = i + 1
	elseif k:pressed(Keyboard.UP) then
		i = i - self.map.tilemap.mapWidth
	elseif k:pressed(Keyboard.DOWN) then
		i = i + self.map.tilemap.mapWidth
	end

	if i ~= bi then
		local ball = self:getEntityAtTile(self.balls, i)
		if ball ~= nil then
			local nbi = i + (i - bi)
			if not self.map:isTileCollidableAt(nbi) and self:getEntityAtTile(self.balls, nbi) == nil then
				self:moveEntityToTile(ball, nbi)
				self:moveEntityToTile(self.player, i)

				local house = self:getEntityAtTile(self.houses, nbi)
				if house ~= nil then
					house:fill()
					ball.visible = false
				else
					ball.visible = true
				end

				house = self:getEntityAtTile(self.houses, i)
				if house ~= nil then
					house:empty()
				end

				self:increaseStepCount()
				self:checkWinCondition()
			end
		elseif not self.map:isTileCollidableAt(i) then
			self:moveEntityToTile(self.player, i)
			self:increaseStepCount()
		end
	end
end

function GameScene:increaseStepCount()
	self.hud:setStepCount(self.hud.steps + 1)
end

function GameScene:moveEntityToTile(entity, index)
	local p = self.map:getPositionAtIndex(index)
	entity.x = p.x
	entity.y = p.y
end

function GameScene:getEntityAtTile(list, index)
	for i=1, list:size(), 1 do
		local e = list:get(i)
		if index == self.map:getIndexAtPosition(Point(e.x, e.y)) then
			return e
		end
	end
	return nil
end

return GameScene