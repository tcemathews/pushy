local Entity = require "lust.Entity"
local BallSprite = require "graphics.BallSprite"

local Ball = Class(Entity, function(self)
	Entity.init(self)
	self.type = "Ball"
	self.sprite = nil
end)

function Ball:added()
	Entity.added(self)
	if self.sprite == nil then
		self.sprite = BallSprite(self.scene.textures)
		self.graphics:push(self.sprite)
	end
end

return Ball