local Entity = require "lust.Entity"
local PlayerSprite = require "graphics.PlayerSprite"

local Player = Class(Entity, function(self)
	Entity.init(self)
	self.type = "Player"
	self.sprite = nil
end)

function Player:added()
	Entity.added(self)
	if self.sprite == nil then
		self.sprite = PlayerSprite(self.scene.textures)
		self.graphics:push(self.sprite)
	end
end

return Player