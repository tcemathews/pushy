local Entity = require "lust.Entity"
local Label = require "lust.graphics.Label"

local Hud = Class(Entity, function(self)
	Entity.init(self)
	self.type = "Hud"
	self.dfont = love.graphics.newFont(12)
	self.stepsLabel = nil
	self.steps = 0
end)

function Hud:added()
	Entity.added(self)
	if self.stepsLabel == nil then
		self.stepsLabel = Label(self.dfont)
		self.graphics:push(self.stepsLabel)
		self:setStepCount(0)
	end
end

function Hud:setStepCount(value)
	self.steps = value
	self.stepsLabel.text = "Steps: " .. self.steps
end

return Hud