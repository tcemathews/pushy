local Entity = require "lust.Entity"
local HouseSprite = require "graphics.HouseSprite"

local House = Class(Entity, function(self)
	Entity.init(self)
	self.type = "House"
	self.sprite = nil
	self.filled = false
end)

function House:added()
	Entity.added(self)
	if self.sprite == nil then
		self.sprite = HouseSprite(self.scene.textures)
		self.graphics:push(self.sprite)
	end
end

function House:fill()
	self.sprite:setFrame(1)
	self.filled = true
end

function House:empty()
	self.sprite:setFrame(0)
	self.filled = false
end

return House