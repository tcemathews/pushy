local Entity = require "lust.Entity"
local Rectangle = require "lust.geometry.Rectangle"
local Point = require "lust.geometry.Point"
local DefaultTilemap = require "graphics.DefaultTilemap"

local Map = Class(Entity, function(self)
	Entity.init(self)
	self.type = "Map"
	self.tilemap = nil
end)

function Map:added()
	Entity.added(self)
	if self.tilemap == nil then
		self.tilemap = DefaultTilemap(self.scene.textures)
		self.graphics:push(self.tilemap)
	end
end

function Map:update(dt)
	Entity:update(self, dt)
end

function Map:getTiles()
	local arr = {}
	local count = self.tilemap.mapWidth * self.tilemap.mapHeight
	for i=1, count, 1 do
		local t = self.tilemap:getTileAtIndex(i - 1)
		if t == nil then t = 0 end
		arr[i] = t
	end
	return arr
end

function Map:setTiles(arr)
	if arr then
		local count = #arr
		for i=1, count, 1 do
			self.tilemap:setTileAtIndex(i, arr[i])
		end
	end
end

function Map:changeTileAt(point)
	local px, py = self.tilemap:xyToPosition(point.x, point.y)
	local t = self.tilemap:getTile(px, py)
	if t ~= nil then 
		t = t + 1
		if t > 3 then
			t = 0
		end
	else
		t = 0
	end
	self.tilemap:setTile(px, py, t)
end

function Map:isTileCollidableAt(index)
	local tile = self.tilemap:getTileAtIndex(index)
	if tile == 1 then
		return true
	end
	return false
end

function Map:getPositionAtIndex(index)
	local x, y = self.tilemap:indexToPosition(index)
	x = x * self.tilemap.tileSize.width
	y = y * self.tilemap.tileSize.height
	return self:localToGlobal(Point(x, y))
end

function Map:getIndexAtPosition(point)
	local p = self:globalToLocal(point)
	local x, y = self.tilemap:xyToPosition(p.y, p.x)
	return self.tilemap:positionToIndex(x, y)
end

return Map