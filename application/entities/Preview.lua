local Entity = require "lust.Entity"
local Sprite = require "lust.graphics.Sprite"
local Label = require "lust.graphics.Label"
local DefaultTilemap = require "graphics.DefaultTilemap"
local PlayerSprite = require "graphics.PlayerSprite"
local BallSprite = require "graphics.BallSprite"
local HouseSprite = require "graphics.HouseSprite"

local Preview = Class(Entity, function(self)
	Entity.init(self)
	self.type = "Preview"
	self.tilemap = nil
	self.player = nil
	self.nameLabel = nil
	self.bestLabel = nil
	self.font = love.graphics.newFont(12)
	self.font:setLineHeight(14)
end)

function Preview:added()
	Entity.added(self)
	if self.tilemap == nil then
		self.tilemap = DefaultTilemap(self.scene.textures)
	end
	if self.player == nil then
		self.player = PlayerSprite(self.scene.textures)
	end
	if self.nameLabel == nil then
		self.nameLabel = Label(self.font)
	end
	if self.bestLabel == nil then
		self.bestLabel = Label(self.font)
	end
end

function Preview:show(name, level)
	self.graphics:empty()

	-- some labling
	self.nameLabel:setText("Level: " .. name)
	self.nameLabel.x = 0
	self.nameLabel.y = 0 - self.nameLabel.height
	self.bestLabel:setText("Best Steps: " .. 0)
	self.bestLabel.x = 0
	self.bestLabel.y = self.tilemap.height
	self.graphics:push(self.nameLabel)
	self.graphics:push(self.bestLabel)

	-- do tiles
	local tiles = level.data.tiles;
	if tiles then
		local count = #tiles
		for i=1, count, 1 do
			self.tilemap:setTileAtIndex(i, tiles[i])
		end
	end

	-- do player
	self.player.x, self.player.y = self:getTilePosition(level.data.playerPosition)

	local balls = level.data.balls
	local houses = level.data.houses

	-- do balls
	if balls then
		local count = #balls
		local hcount = #houses or 0
		for i=1, count, 1 do
			local skip = false
			for n=1, hcount, 1 do
				if balls[i] == houses[n] then
					skip = true
					break
				end
			end
			if skip == false then
				local ball = BallSprite(self.scene.textures)
				ball.x, ball.y = self:getTilePosition(balls[i])
				self.graphics:push(ball)
			end
		end
	end

	-- do houses
	if houses then
		local count = #houses
		local bcount = #balls or 0
		for i=1, count, 1 do
			local house = HouseSprite(self.scene.textures)
			house.x, house.y = self:getTilePosition(houses[i])
			self.graphics:push(house)
			for n=1, bcount, 1 do
				if balls[n] == houses[i] then
					house:setFrame(1)
				end
			end
		end
	end

	self.graphics:push(self.player)
	self.graphics:push(self.tilemap)
end

function Preview:getTilePosition(index)
	local x, y = self.tilemap:indexToPosition(index)
	x = x * self.tilemap.tileSize.width
	y = y * self.tilemap.tileSize.height
	return x, y
end

return Preview