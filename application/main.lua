local GameScene = require "scenes.GameScene"
local LevelSelectScene = require "scenes.LevelSelectScene"
local Process = require "lust.Process"

function love.load()
	love.graphics.setBackgroundColor(255, 255, 255)
	process = Process()
	process:show(LevelSelectScene())
end

function love.update(dt)
	process:update(dt)
end

function love.draw()
	process:render()
	love.graphics.setColor(0, 0, 0, 255)
	love.graphics.print("FPS:"..tostring(love.timer.getFPS()), 0, 0)
end

function love.mousepressed(x, y, mbtn)
	process:mousePressed(x, y, mbtn)
end

function love.keypressed(key, unicode) 
	process:keyPressed(key, unicode)
	if key == "escape" then
		love.event.quit()
	end
end

function love.keyreleased(key, unicode)
	process:keyReleased(key, unicode)
end