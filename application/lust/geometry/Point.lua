require 'lust.utilities.Class'

local Point = Class(function(self, x, y)
	self.x = x or 0
	self.y = y or 0
end)

function Point:clone()
	return Point(self.x, self.y)
end

function Point:copy(point)
	self.x = point.x
	self.y = point.y
end

return Point