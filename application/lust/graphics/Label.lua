local Graphic = require "lust.graphics.Graphic"

local Label = Class(Graphic, function(self, font)
	Graphic.init(self)
	self.text = ""
	self.width = 100
	self.alignment = nil
	self.font = nil
	self.color = {0, 0, 0, 255}
	self:setFont(font)
	self:alignLeft()
end)

Label.ALIGN_RIGHT = "right"
Label.ALIGN_LEFT = "left"
Label.ALIGN_CENTER = "center"

function Label:draw(ox, oy)
	Graphic.draw(self, ox, oy)
	if self.font ~= nil then
		love.graphics.setFont(self.font)
		love.graphics.printf(self.text, ox + self.x, oy + self.y, self.width, self.alignment)
	end
end

function Label:setText(value)
	if value == nil then value = "" end
	self.text = value
	self:updateDimensions()
end

function Label:alignLeft()
	self.alignment = Label.ALIGN_LEFT
	self:updateDimensions()
end

function Label:alignRight()
	self.alignment = Label.ALIGN_RIGHT
	self:updateDimensions()
end

function Label:alignCenter()
	self.alignment = Label.ALIGN_CENTER
	self:updateDimensions()
end

function Label:setFont(font)
	self.font = font
	self:updateDimensions()
end

function Label:updateDimensions()
	if self.font ~= nil then
		local width, lines = self.font:getWrap(self.text, self.width)
		self.height = lines * self.font:getLineHeight()
	end
end

return Label