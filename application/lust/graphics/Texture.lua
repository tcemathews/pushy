require 'lust.utilities.Class'

local Texture = Class(function(self, value)
	self.image = nil
	self.width = 0
	self.height = 0
	self:setImage(value)
end)

function Texture:setImage(value)
	if type(value) == "string" or value:type() == "ImageData" or value:type() == "File" then
		self.image = love.graphics.newImage(value)
	elseif value:type() == "Image" then
		self.image = value
	else
		self.image = nil
	end

	if self.image ~= nil then
		self.width = self.image:getWidth()
		self.height = self.image:getHeight()
	else
		self.width = 0
		self.height = 0
	end
end

function Texture:setSmoothing(value)
	if self.image then
		smoothing = "nearest"
		if value then smoothing = "linear" end
		self.image:setFilter(smoothing, smoothing)
	end
end

return Texture