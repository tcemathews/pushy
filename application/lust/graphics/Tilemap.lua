Graphic = require "lust.graphics.Graphic"

local Tilemap = Class(Graphic, function(self)
	Graphic.init(self)
	self.mapWidth = 0
	self.mapHeight = 0
	self.texture = nil
	self.tileSize = {width=0, height=0}
	self.batch = nil
	self.tiles = nil
	self.cache = {}
	self.idCache = {}
end)

function Tilemap:set(texture, tileWidth, tileHeight, mapWidth, mapHeight)
	self.texture = texture
	self.tileSize = {width=tileWidth, height=tileHeight}
	self.mapWidth = mapWidth
	self.mapHeight = mapHeight
	self.tiles = {}
	self.batch = love.graphics.newSpriteBatch(self.texture.image, self.mapWidth * self.mapHeight)
	self.width = self.mapWidth * self.tileSize.width
	self.height = self.mapHeight * self.tileSize.height
end

function Tilemap:addTile(key, x, y)
	self.tiles[key] = love.graphics.newQuad(x * self.tileSize.width, y * self.tileSize.height, self.tileSize.width, self.tileSize.height, self.texture.width, self.texture.height)
end

function Tilemap:setTile(x, y, key)
	if key == nil or self.tiles[key] == nil then return end

	local id = self:getTileId(x, y)
	if self.idCache[id] == nil then
		self.idCache[id] = self.batch:addq(self.tiles[key], x * self.tileSize.width, y * self.tileSize.height)
	else
		self.batch:setq(self.idCache[id], self.tiles[key], x * self.tileSize.width, y * self.tileSize.height)
	end
	self.cache[id] = key
end

function Tilemap:setTileAtIndex(index, key)
	local x, y = self:indexToPosition(index - 1)
	self:setTile(x, y, key)
end

function Tilemap:getTile(x, y)
	local id = self:getTileId(x, y)
	local tile = nil
	if self.cache[id] then 
		tile = self.cache[id]
	end
	return tile
end

function Tilemap:getTileAtIndex(index)
	local x, y = self:indexToPosition(index)
	return self:getTile(x, y)
end

function Tilemap:getTileId(x, y)
	return x .. "_" .. y
end

function Tilemap:positionToIndex(x, y)
	return x * self.mapWidth + y
end

function Tilemap:indexToPosition(index)
	if index == nil then index = 0 end
	local x = math.floor(index % self.mapWidth)
	local y = math.floor(index / self.mapWidth)
	return x, y
end

function Tilemap:xyToPosition(x, y)
	x = math.floor(x / self.tileSize.width)
	y = math.floor(y / self.tileSize.height)
	return x, y
end

function Tilemap:draw(ox, oy)
	Graphic.draw(self, ox, oy)
	if self.batch then
		love.graphics.draw(self.batch, ox + self.x, oy + self.y, self.rotation, self.scale.x, self.scale.y, self.origin.x, self.origin.y, self.shear.x, self.shear.y)
	end
end

return Tilemap