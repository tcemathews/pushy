require 'lust.utilities.Class'

local List = Class(function(self)
	self.items = {}
end)

function List:size()
	return #self.items
end

function List:get(index)
	return self.items[index]
end

function List:setElementIndex(element, nindex)
	-- Make sure we add to the end of the list.
	if nindex > self:size() then
		nindex = self:size() + 1
	end

	-- Make sure we add to the beginning of the list.
	if nindex < 0 then
		nindex = 0
	end

	-- If the element exists take it out.
	index = self:indexOf(element)
	if index > 0 then
		self:slice(index)
	end

	table.insert(self.items, nindex, element)
end

function List:indexOf(element)
	for key, value in pairs(self.items) do
		if value == element then
			return key
		end
	end
	return 0
end

function List:push(element)
	table.insert(self.items, element)
end

function List:pop()
	return table.remove(self.items)
end

function List:slice(index)
	return table.remove(self.items, index)
end

function List:shift()
	return table.remove(self.items, 1)
end

function List:empty()
	while #self.items > 0 do
		self:pop()
	end
end

return List