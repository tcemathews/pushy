require 'lust.utilities.Class'

local Cache = Class(function(self, value)
	self:empty()
end)

function Cache:empty()
	self.cache = {}
end

function Cache:get(key)
	return self.cache[key]
end

function Cache:set(key, value)
	self.cache[key] = value
end

return Cache