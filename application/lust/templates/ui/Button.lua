local View = require "lust.templates.ui.View"
local Sprite = require "lust.graphics.Sprite"
local Rectangle = require "lust.geometry.Rectangle"
local Point = require "lust.geometry.Point"

local Button = Class(View, function(self, picture)
	View.init(self)
	self.type = "Button"
	self.hitbox = Rectangle(0, 0, 0, 0)
	self.clicked = false

	if picture then
		self.picture = picture
		self.graphics:push(self.picture)
		self.frame.width = picture.width
		self.frame.height = picture.height
		self.hitbox.width = picture.width
		self.hitbox.height = picture.height
	end
end)

function Button:update(dt)
	View:update(self, dt)
	self.clicked = false
	self:checkContact(self.scene.mouse.left, self:getGlobalHitbox())
end

function Button:getGlobalHitbox()
	local rect = self.hitbox:clone()
	rect.x = rect.x + self.x - self.scene.camera.x
	rect.y = rect.y + self.y - self.scene.camera.y
	return rect
end

function Button:checkContact(contact, rect)
	if rect:pointInside(contact.position) and not contact.preventsNext then
		if contact.activated then
			self:onDown()
			contact:preventNext()
		elseif contact.deactivated then
			self:onUp()
			contact:preventNext()
		end
	end
end

function Button:onDown()
end

function Button:onUp()
	self.clicked = true
end

return Button