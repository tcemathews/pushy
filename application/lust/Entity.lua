require 'lust.utilities.Class'

local List = require 'lust.utilities.List'
local Point = require 'lust.geometry.Point'

local Entity = Class(function(self)
	self.type = nil
	self.x = 0
	self.y = 0
	self.graphics = List()
	self.scene = nil
	self.visible = true
end)

function Entity:added()
	-- The scene added this entity to itself, the scene variable has been replaced.
end

function Entity:removed()
	-- The scene has removed the entity from itself, the scene variable has been cleared.
end

function Entity:update(dt)
	-- Do update operations here.
end

function Entity:render()
	if self:onCamera() and self.visible then
		for i = self.graphics:size(), 1, -1 do
			graphic = self.graphics:get(i)
			if graphic.visible then
				graphic:draw(self.scene.camera.x + self.x, self.scene.camera.y + self.y)
			end
		end
	end
end

function Entity:onCamera()
	return self.scene:onCamera(self.x, self.y)
end

function Entity:globalToLocal(point)
	local p = point:clone()
	p.x = p.x - self.x
	p.y = p.y - self.y
	return p
end

function Entity:localToGlobal(point)
	local p = point:clone()
	p.x = p.x + self.x
	p.y = p.y + self.y
	return p
end

return Entity