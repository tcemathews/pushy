local Sprite = require "lust.graphics.Sprite"

local PlayerSprite = Class(Sprite, function(self, textures)
	Sprite.init(self)
	local texture = textures:aget("assets/player.png")
	self:addFrame(0, texture, 0, 0, texture.width, texture.height)
	self:setFrame(0)
end)

return PlayerSprite