local Sprite = require "lust.graphics.Sprite"

local BallSprite = Class(Sprite, function(self, textures)
	Sprite.init(self)
	local texture = textures:aget("assets/ball.png")
	self:addFrame(0, texture, 0, 0, texture.width, texture.height)
	self:setFrame(0)
end)

return BallSprite