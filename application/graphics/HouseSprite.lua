local Sprite = require "lust.graphics.Sprite"

local HouseSprite = Class(Sprite, function(self, textures)
	Sprite.init(self)
	local texture = textures:aget("assets/house.png")
	self:addFrame(0, texture, 0, 0, 10, 10)
	self:addFrame(1, texture, 10, 0, 10, 10)
	self:setFrame(0)
end)

return HouseSprite