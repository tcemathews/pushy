local Tilemap = require "lust.graphics.Tilemap"

local DefaultTilemap = Class(Tilemap, function(self, textures)
	Tilemap.init(self)
	self:set(textures:aget("assets/tileset.png"), 10, 10, 11, 11)
	self:addTile(0, 0, 0)
	self:addTile(1, 1, 0)
	self:addTile(2, 2, 0)
	self:addTile(3, 3, 0)
end)

return DefaultTilemap